/******************************************************************************
 *
 *  This file is part of the Data Logging Service (DLS).
 *
 *  DLS is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <algorithm>

#include <FL/Fl.H>
#include <FL/fl_ask.H>

/*****************************************************************************/

#include "lib/XmlParser.h"
#include "lib/RingBufferT.h"

#include "CtlGlobals.h"
#include "CtlDialogChannels.h"
#include "CtlDialogMsg.h"

#include <pdcom5/Exception.h>
#include <pdcom5/Process.h>
#include <pdcom5/PosixProcess.h>
#include <pdcom5/Variable.h>
#include <pdcom5/SizeTypeInfo.h>

using std::stringstream;

/*****************************************************************************/

#define WIDTH 550
#define HEIGHT 350

/*****************************************************************************/

namespace {

class ChannelFetcher : public PdCom::Process, public PdCom::PosixProcess
{


public:
    bool running_ = true;
    std::vector<PdCom::Variable> variables_;

    ChannelFetcher(const char *host, unsigned short port) :
        PdCom::Process(),
        PdCom::PosixProcess(host, port)
    {}

    void run_until_complete()
    {
        while (running_)
            asyncData();
    }


private:

    void connected() override
    {
        list();
    }

    void listReply(std::vector<PdCom::Variable> variables,
                   std::vector<std::string> /* dirs */) override
    {
        variables_ = std::move(variables);
        running_ = false;
    }


    int read(char *buf, int count) override
    {
        const int ans = posixRead(buf, count);
        // stop on EOF
        if (ans == 0)
            running_ = false;
        return ans;
    }
    void write(const char *buf, size_t count) override
    {
        posixWriteBuffered(buf, count);
    }
    void flush() override { posixFlush(); }
};

LibDLS::ChannelType convert_pdcom_datatype(const PdCom::TypeInfo::DataType dt)
{
    using DT = PdCom::TypeInfo::DataType;
    switch(dt) {
    case DT::char_T:
    case DT::int8_T:
        return LibDLS::TCHAR;
    case DT::uint8_T:
        return LibDLS::TUCHAR;
    case DT::int16_T:
        return LibDLS::TSHORT;
    case DT::uint16_T:
        return LibDLS::TUSHORT;
    case DT::int32_T:
        return LibDLS::TINT;
    case DT::uint32_T:
        return LibDLS::TUINT;
    case DT::int64_T:
        return LibDLS::TLINT;
    case DT::uint64_T:
        return LibDLS::TULINT;
    case DT::single_T:
        return LibDLS::TFLT;
    case DT::double_T:
        return LibDLS::TDBL;
    default:
    return LibDLS::TUNKNOWN;
    }
}

bool fetch_channels(
        std::vector<LibDLS::RealChannel> &channels,
        std::string& err,
        const char *host,
        unsigned short port)
{
    try {
        ChannelFetcher fetcher(host, port);
        fetcher.run_until_complete();
        for (const auto& pdcom_channel : fetcher.variables_)
        {
            LibDLS::RealChannel channel = {};
            channel.name = pdcom_channel.getPath();
            if (const auto st = pdcom_channel.getSampleTime().count())
                channel.frequency = 1.0 / st;
            else
                continue;
            channel.type = convert_pdcom_datatype(pdcom_channel.getTypeInfo().type);
            if (channel.type == LibDLS::TUNKNOWN
                or !pdcom_channel.getSizeInfo().isScalar())
                continue;
            channels.push_back(channel);
        }

    } catch(const PdCom::Exception& e) {
        err = e.what();
        channels.clear();
        return false;
    }
    return true;
}

} // namespace

/*****************************************************************************/

/**
   Constructor

   \param source IP-Address or hostname of the data source
*/

CtlDialogChannels::CtlDialogChannels(const std::string &source, uint16_t port)
{
    int x = Fl::w() / 2 - WIDTH / 2;
    int y = Fl::h() / 2 - HEIGHT / 2;

    _source = source;
    _port = port;
    _thread_running = false;

    _wnd = new Fl_Double_Window(x, y, WIDTH, HEIGHT, "Select channels");
    _wnd->set_modal();
    _wnd->callback(_callback, this);

    _button_ok = new Fl_Button(WIDTH - 90, HEIGHT - 35, 80, 25, "OK");
    _button_ok->deactivate();
    _button_ok->callback(_callback, this);
    _button_cancel = new Fl_Button(WIDTH - 180, HEIGHT - 35, 80, 25,
                                   "Cancel");
    _button_cancel->callback(_callback, this);

    _checkbutton_reduceToOneHz = new Fl_Check_Button(10, HEIGHT - 35,
            WIDTH - 200, 25, "Reduce to 1 Hz");
    _checkbutton_reduceToOneHz->set();

    _grid_channels = new Fl_Grid(10, 10, WIDTH - 20, HEIGHT - 55);
    _grid_channels->add_column("name", "Channel", 200);
    _grid_channels->add_column("unit", "Unit", 50);
    _grid_channels->add_column("freq", "Sampling rate (Hz)");
    _grid_channels->add_column("type", "Type");
    _grid_channels->select_mode(flgMultiSelect);
    _grid_channels->callback(_callback, this);
    _grid_channels->hide();

    _box_message = new Fl_Box(10, 10, WIDTH - 20, HEIGHT - 55);
    _box_message->align(FL_ALIGN_CENTER);
    _box_message->label("Receiving channels...");

    _wnd->end();
    _wnd->resizable(_grid_channels);
}

/*****************************************************************************/

/**
   Destructor
*/

CtlDialogChannels::~CtlDialogChannels()
{
    delete _wnd;
}

/*****************************************************************************/

/**
   Static callback function

   \param sender Widget that triggered the callback
   \param data Pointer to the dialog
*/

void CtlDialogChannels::_callback(Fl_Widget *sender, void *data)
{
    CtlDialogChannels *dialog = (CtlDialogChannels *) data;

    if (sender == dialog->_grid_channels) dialog->_grid_channels_callback();
    if (sender == dialog->_button_ok) dialog->_button_ok_clicked();
    if (sender == dialog->_button_cancel) dialog->_button_cancel_clicked();
    if (sender == dialog->_wnd) dialog->_button_cancel_clicked();
}

/*****************************************************************************/

/**
   Callback of the channels grids
*/

void CtlDialogChannels::_grid_channels_callback()
{
    unsigned int i;
    stringstream str;

    switch (_grid_channels->current_event())
    {
        case flgContent:
            i = _grid_channels->current_record();

            if (_grid_channels->current_col() == "name")
            {
                _grid_channels->current_content(_channels[i].name);
            }
            else if (_grid_channels->current_col() == "unit")
            {
                _grid_channels->current_content(_channels[i].unit);
            }
            else if (_grid_channels->current_col() == "freq")
            {
                str << _channels[i].frequency;
                _grid_channels->current_content(str.str());
            }
            else if (_grid_channels->current_col() == "type")
            {
                _grid_channels->current_content(
                    channel_type_to_str(_channels[i].type));
            }
            break;

        case flgSelect:
            _button_ok->activate();
            break;

        case flgDeSelect:
            _button_ok->deactivate();
            break;

        case flgDoubleClick:
            _button_ok_clicked();
            break;

        default:
            break;
    }
}

/*****************************************************************************/

/**
   Callback: The "OK" button was clicked
*/

void CtlDialogChannels::_button_ok_clicked()
{
    std::list<unsigned int>::const_iterator sel_i;

    // Maybe cancel the thread
    if (_thread_running)
    {
        pthread_cancel(_thread);
    }

    // Create a list of selected channels
    _selected.clear();
    sel_i = _grid_channels->selected_list()->begin();
    while (sel_i != _grid_channels->selected_list()->end())
    {
        if (_checkbutton_reduceToOneHz->value()){
            _channels[*sel_i].frequency=1;
        }

        _selected.push_back(_channels[*sel_i]);
        sel_i++;
    }

    // Close the window
    _wnd->hide();
}

/*****************************************************************************/

/**
   Callback: The "Cancel" button was clicked
*/

void CtlDialogChannels::_button_cancel_clicked()
{
    // Maybe cancel the thread
    if (_thread_running)
    {
        pthread_cancel(_thread);
    }

    // Cancel = no channels selected
    _selected.clear();

    // Close the window
    _wnd->hide();
}

/*****************************************************************************/

/**
   Show dialog
*/

void CtlDialogChannels::show()
{
    if (pthread_create(&_thread, 0, _static_thread_function, this) == 0)
    {
        _wnd->show();

        while (_wnd->shown()) Fl::wait();
    }
    else
    {
        msg_win->str() << "Failed to start a new thread!";
        msg_win->error();
    }
}

/*****************************************************************************/

void *CtlDialogChannels::_static_thread_function(void *data)
{
    CtlDialogChannels *dialog = (CtlDialogChannels *) data;

    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, 0);

    Fl::lock();
    dialog->_error = "";
    dialog->_thread_running = true;
    Fl::unlock();

    dialog->_thread_function();

    Fl::lock();
    dialog->_thread_running = false;
    dialog->_thread_finished();
    Fl::unlock();

    return 0;
}

/*****************************************************************************/

void CtlDialogChannels::_thread_function()
{
    fetch_channels(_channels, _error, _source.c_str(), _port);
}

/*****************************************************************************/

void CtlDialogChannels::_thread_finished()
{
    _box_message->hide();
    _grid_channels->show();
    _grid_channels->take_focus();

    if (_error != "")
    {
        msg_win->str() << _error;
        msg_win->error();

        _wnd->hide();
    }
    else if (_channels.size() > 0)
    {
        sort(_channels.begin(), _channels.end());
        _grid_channels->record_count(_channels.size());
    }
}

/*****************************************************************************/
