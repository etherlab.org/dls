/******************************************************************************
 *
 *  This file is part of the Data Logging Service (DLS).
 *
 *  DLS is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "ZLib.h"

#include "LibDLS/globals.h"

#include <sstream>
#include <zlib.h>

using namespace LibDLS;

//#define DEBUG

ZLib::ZLib()  = default;
ZLib::~ZLib() = default;


class ZLib::Impl
{
  public:
    ~Impl() { free(); }

    int
    compress(Bytef *dest, uLongf *destLen, const Bytef *source, uLong sourceLen)
    {
        const uInt max = (uInt) -1;
        uLong left;
        int err = 0;
        setDirection(Direction::Compress);

        stream_.next_out  = dest;
        stream_.avail_out = 0;
        stream_.next_in   = const_cast<Bytef *>(source);
        stream_.avail_in  = 0;

        err = deflateReset(&stream_);
        if (err != Z_OK)
            return err;

        do {
            if (stream_.avail_out == 0) {
                stream_.avail_out = left > (uLong) max ? max : (uInt) left;
                left -= stream_.avail_out;
            }
            if (stream_.avail_in == 0) {
                stream_.avail_in =
                        sourceLen > (uLong) max ? max : (uInt) sourceLen;
                sourceLen -= stream_.avail_in;
            }
            err = deflate(&stream_, sourceLen ? Z_NO_FLUSH : Z_FINISH);
        } while (err == Z_OK);

        *destLen = stream_.total_out;
        return err == Z_STREAM_END ? Z_OK : err;
    }

    int uncompress(
            Bytef *dest,
            uLongf *destLen,
            const Bytef *source,
            uLong sourceLen)
    {
        setDirection(Direction::Uncompress);
        int err;
        const uInt max = (uInt) -1;
        uLong len, left;
        Byte buf[1]; /* for detection of incomplete stream when *destLen == 0 */

        len = sourceLen;

        if (*destLen) {
            left     = *destLen;
            *destLen = 0;
        }
        else {
            left = 1;
            dest = buf;
        }

        stream_.next_in  = (z_const Bytef *) source;
        stream_.avail_in = 0;
        stream_.next_out  = dest;
        stream_.avail_out = 0;

        err = inflateReset(&stream_);
        if (err != Z_OK)
            return err;

        do {
            if (stream_.avail_out == 0) {
                stream_.avail_out = left > (uLong) max ? max : (uInt) left;
                left -= stream_.avail_out;
            }
            if (stream_.avail_in == 0) {
                stream_.avail_in = len > (uLong) max ? max : (uInt) len;
                len -= stream_.avail_in;
            }
            err = inflate(&stream_, Z_NO_FLUSH);
        } while (err == Z_OK);

        if (dest != buf)
            *destLen = stream_.total_out;
        else if (stream_.total_out && err == Z_BUF_ERROR)
            left = 1;

        return err == Z_STREAM_END                               ? Z_OK
                : err == Z_NEED_DICT                             ? Z_DATA_ERROR
                : err == Z_BUF_ERROR && left + stream_.avail_out ? Z_DATA_ERROR
                                                                 : err;
    }

  private:
    z_stream stream_ = {};
    enum class Direction {
        None,
        Compress,
        Uncompress,
    } dir_ = Direction::None;

    void setDirection(Direction dir)
    {
        if (dir_ == dir)
            return;
        free();
        init(dir);
    }

    void init(Direction dir)
    {
        int err = 0;
        if (dir == Direction::Compress) {
            err = deflateInit(&stream_, Z_DEFAULT_COMPRESSION);
        }
        else {
            err = inflateInit(&stream_);
        }
        if (err != Z_OK)
            throw EZLib("could not initialize ZLib");
        dir_ = dir;
    }

    void free()
    {
        if (dir_ == Direction::Compress) {
            deflateEnd(&stream_);
        }
        else if (dir_ == Direction::Uncompress) {
            inflateEnd(&stream_);
        }
        dir_ = Direction::None;
    }
};

/*****************************************************************************/

/**
   Gibt alle Speicherbereiche vorübergehend frei
*/

void ZLib::free()
{
    _out_buf = {};
}

/*****************************************************************************/

/**
   Komprimieren von Daten

   Komprimiert die Daten im Eingabepuffer und speichert das
   Ergebnis im internen Ausgabepuffer.

   \param src Konstanter zeiger auf die zu komprimierenden Daten
   \param src_size Anzahl der zu komprimierenden Zeichen
   \throw EZLib Fehler beim Komprimieren
*/

void ZLib::compress(const char *src, unsigned int src_size)
{
    uLongf out_size;
    int comp_ret;
    std::stringstream err;

    free();

    // Keine Daten - Nichts zu komprimieren
    if (!src_size) {
        return;
    }

    // Die komprimierten Daten können in sehr ungünstigen Fällen
    // größer sein, als die Quelldaten. Siehe ZLib-Doku:
    // http://www.gzip.org/zlib/manual.html#compress
    out_size = (uLongf) (src_size * 1.01 + 12 + 0.5);

    try
    {
        _out_buf.resize(out_size);
    }
    catch (...)
    {
        err << "Could not allocate " << out_size << " bytes!";
        throw EZLib(err.str());
    }

    if (!impl_)
        impl_.reset(new Impl());

    // Komprimieren
    comp_ret = impl_->compress(
            (Bytef *) _out_buf.data(), &out_size, (const Bytef *) src,
            src_size);

    if (comp_ret != Z_OK) // Fehler beim Komprimieren
    {
        err << "compress() returned " << comp_ret;
        if (comp_ret == Z_BUF_ERROR) err << " (BUFFER ERROR)";
        err << ", out_size=" << out_size;
        err << ", src_size=" << src_size;
        throw EZLib(err.str());
    }

#ifdef DEBUG
    msg() << "ZLib compression: input=" << src_size;
    msg() << " output=" << out_size;
    msg() << " quote=" << (float) out_size / src_size * 100 << "%";
    log(DLSDebug);
#endif

    _out_buf.resize(out_size);
}

/*****************************************************************************/

/**
   Dekomprimieren

   \param src Konstanter Zeiger auf die komprimierten Daten
   \param src_size Größe der komprimierten Daten in Bytes
   \param out_size Erwartete Größe der dekomprimierten Daten in Bytes
   \throw EZLib Fehler beim Dekomprimieren
*/

void ZLib::uncompress(const char *src, unsigned int src_size,
                         unsigned int out_size)
{
    int uncomp_ret;
    std::stringstream err;
    uLongf zlib_out_size = out_size;

    _out_buf.resize(0);

    // Keine Eingabedaten - keine Dekompression
    if (!src_size) return;

    try
    {
        _out_buf.resize(out_size);
    }
    catch (...)
    {
        err << "Could not allocate " << out_size << " bytes!";
        throw EZLib(err.str());
    }
    if (!impl_)
        impl_.reset(new Impl());

    // Dekomprimieren
    uncomp_ret = ::uncompress(
            (Bytef *) _out_buf.data(), &zlib_out_size, (const Bytef *) src,
            src_size);

    if (uncomp_ret != Z_OK) // Fehler beim Dekomprimieren
    {
        err << "uncompress() returned " << uncomp_ret;
        if (uncomp_ret == Z_BUF_ERROR) {
            err << " (BUFFER ERROR)";
        }
        err << ", out_size=" << out_size;
        err << ", src_size=" << src_size;
        throw EZLib(err.str());
    }

    _out_buf.resize(out_size);
}

/*****************************************************************************/
