/******************************************************************************
 *
 *  This file is part of the Data Logging Service (DLS).
 *
 *  DLS is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

/**
 * @file
 *
 * Global data structures and functions.
 */

#ifndef LibDLSGlobalsH
#define LibDLSGlobalsH

/*****************************************************************************/

#include <string>
#include <stdint.h>

/*****************************************************************************/

/** Name of the PID file in the data directory.
 */
#define DLS_PID_FILE "dlsd.pid"

/** Name of the environment variable containing the DLS data directory.
 */
#define ENV_DLS_DIR  "DLS_DIR"

/** Name of the environment variable containing the DLS user.
 */
#define ENV_DLS_USER "DLS_USER"

/*****************************************************************************/

#ifdef _WIN32
/** Macro for public method definitions.
 */
#define DLS_PUBLIC __declspec(dllexport)
#else
/** Macro for public method definitions (empty on non-win32).
 */
#define DLS_PUBLIC
#endif

/*****************************************************************************/

namespace LibDLS {

/*****************************************************************************/

/** Basic DLS channel datatype.
 *
 * When adding data types, the following places have to be modified:
 * - The two conversion functions in lib/globals.cpp
 * - In daemon/Logger::_create_gen_saver()
 * - In fltkguis/ViewChannel::fetch_chunks()
 */
enum ChannelType
{
    TUNKNOWN,
    TCHAR,
    TUCHAR,
    TSHORT,
    TUSHORT,
    TINT,
    TUINT,
    TLINT,
    TULINT,
    TFLT,
    TDBL
};

/** Convert a string to a channel type enum.
 */
DLS_PUBLIC ChannelType str_to_channel_type(const std::string &);

/** Convert a channel type enum to a string.
 */
DLS_PUBLIC const char *channel_type_to_str(ChannelType);

/*****************************************************************************/

/** Meta type for recorded data.
 *
 * When extending, please keep in mind to also change the switch statements in
 * _meta_value() and _ending().
 */
enum MetaType
{
    MetaGen = 0, /**< Generic data as received from data source. */
    MetaMean = 1, /**< Arithmetic mean values (deprecated). */
    MetaMin = 2, /**< Minimum values. */
    MetaMax = 4 /**< Maximum values. */
};

/** Get string from MetaType.
 */
DLS_PUBLIC std::string meta_type_str(MetaType);

/*****************************************************************************/

/** Describes a signal of the data source.
 */
struct RealChannel
{
    std::string name; /**< Signal name. */
    std::string unit; /**< Unit. */
    int index; /**< Index. */
    ChannelType type; /**< Signal data type (TUINT, TDBL, etc.) */
    unsigned int bufsize; /**< Size of the signal buffer. */
    unsigned int frequency; /**< Maximum sample frequency of the signal. */
};

/** Less operator for sorting RealChannel lists.
 */
DLS_PUBLIC bool operator<(const RealChannel &a, const RealChannel &b);

/*****************************************************************************/

/** Compression formats.
 *
 * Adding new compression types:
 * - globals.cpp    (string array)
 * - CompressionT.h (new class)
 */
enum {
	FORMAT_INVALID = -1,
	FORMAT_ZLIB,
	FORMAT_MDCT,
	FORMAT_QUANT,
	FORMAT_COUNT
};

/** String from format enum.
 */
extern const char *format_strings[FORMAT_COUNT];

/*****************************************************************************/

#pragma pack(push, 1)

/** Index record for a data file index as part of a chunk.
 */
struct IndexRecord
{
    uint64_t start_time;
    uint64_t end_time;
    uint32_t position;
};

/*****************************************************************************/

/** Index record for all data file of a chunk.
 */
struct GlobalIndexRecord
{
    uint64_t start_time;
    uint64_t end_time;
};

/*****************************************************************************/

/** Message index.
 */
struct MessageIndexRecord
{
    uint64_t time;
    uint32_t position;
};

/*****************************************************************************/

/** Channel index record, containing all chunk times.
 */
struct ChannelIndexRecord
{
    uint64_t start_time;
    uint64_t end_time;
};

#pragma pack(pop)

/*****************************************************************************/

/** Protocol definitions. */
enum {
    MSR_PORT = 2345
};

/*****************************************************************************/

/** Logging callback type.
 *
 * Function pointer type to use with set_logging_callback().
 * The second parameter will be the private pointer given in
 * set_logging_callback().
 */
typedef void (*LoggingCallback)(const char *, void *);

/** Set global logging callback.
 *
 * If no logging callback is set, all library log messages got to STDERR.
 */
DLS_PUBLIC void set_logging_callback(LoggingCallback, void *);

/** Global log method, that is used by the library.
 */
DLS_PUBLIC void log(const std::string &);

/** Convert data to binary.
 */
DLS_PUBLIC std::string convert_to_bin(const void *, unsigned int, int);

/*****************************************************************************/

} // namespace

/*****************************************************************************/

#endif
