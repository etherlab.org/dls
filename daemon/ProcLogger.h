/******************************************************************************
 *
 *  This file is part of the Data Logging Service (DLS).
 *
 *  DLS is free software: you can redistribute it and/or modify it under the
 *  terms of the GNU General Public License as published by the Free Software
 *  Foundation, either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 *  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef ProcLoggerH
#define ProcLoggerH

/*****************************************************************************/

#include <string>
#include <list>
#include <sstream>
#include <vector>

/*****************************************************************************/

#include <pdcom5/MessageManagerBase.h>
#include <pdcom5/Process.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>

/*****************************************************************************/

#include "lib/XmlParser.h"
#include "lib/LibDLS/Time.h"

#include "Job.h"
#include "VariableAssignTool.h"

/*****************************************************************************/

/**
   Logging-Prozess
*/

class ProcLogger;

class FindProxy
{
    friend class ProcLogger;

    ProcLogger * const process_;
    explicit FindProxy(ProcLogger *process) :
    process_(process) {}

    FindProxy& operator=(FindProxy const&) = delete;
    FindProxy& operator=(FindProxy &&) = delete;
    FindProxy(FindProxy const&) = delete;
    FindProxy(FindProxy&&) = delete;

    public:

    bool find(const std::string& path) const;
};

class ProcLogger:
    public PdCom::Process,
    private PdCom::MessageManagerBase,
    private PdCom::Subscriber // for trigger variable
{
public:
    ProcLogger(const std::string &);
    ~ProcLogger();

    int start(unsigned int);

    void notify_error(int);
    void notify_data(void);

    std::string dls_dir() const { return _dls_dir; }

private:
    std::string _dls_dir;
    Job _job;
    int _socket;
    unsigned int _sig_hangup;
    unsigned int _sig_child;
    unsigned int _sig_usr1;
    bool _exit;
    int _exit_code;
    enum {
        Connecting,
        Waiting,
        Data
    } _state;
    LibDLS::Time _quota_start_time;
    LibDLS::Time _last_watchdog_time;
    LibDLS::Time _last_receive_time;
    bool _receiving_data;
    PdCom::Subscription _trigger;
    std::vector<char> _write_buffer;
    std::unique_ptr<VariableAssignTool> _variable_assign_tool;

    void _start(unsigned int);
    bool _connect_socket();
    void _read_write_socket();
    void _subscribe_trigger();
    void _check_signals();
    void _reload();
    void _do_watchdogs();
    void _do_quota();
    void _create_pid_file();
    void _remove_pid_file();
    void _flush();

    // PdCom::Process
    std::string applicationName() const override;
    std::string hostname() const override;
    void connected() override;
    int read(char *buf, int bufsize) override;
    void write(const char *buf, size_t count) override;
    void flush() override;
    void findReply(PdCom::Variable const& var) override;
    void broadcastReply(
            const std::string &message,
            const std::string &attr,
            std::chrono::nanoseconds time_ns,
            const std::string &user);

    // PdCom::MessageManagerBase
    void processMessage(PdCom::Message) override;

    // from PdCom::Subscriber()
    void newValues(std::chrono::nanoseconds) override;
    void stateChanged(PdCom::Subscription const&) override;

    friend class FindProxy;
};

/*****************************************************************************/


inline bool FindProxy::find(const std::string& path) const
{
    return process_->find(path);
}
#endif
