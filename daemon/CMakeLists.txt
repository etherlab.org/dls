#-----------------------------------------------------------------------------
#
# Copyright (C) 2012 - 2017  Florian Pose <fp@igh-essen.com>
#               2022         Bjarne von Horn <vh at igh dot de>
#
# This file is part of the data logging service (DLS).
#
# DLS is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# DLS is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with the DLS. If not, see <http://www.gnu.org/licenses/>.
#
#-----------------------------------------------------------------------------

set(DLSD_SOURCES
    globals.cpp
    Job.cpp
    JobPreset.cpp
    Logger.cpp
    main.cpp
    Message.cpp
    MessageList.cpp
    ProcLogger.cpp
    ProcMother.cpp
)

if (DLS_SERVER)
    protobuf_generate_cpp(PROTO_SRC PROTO_HDR ../proto/dls.proto)
    list(APPEND DLSD_SOURCES
        Connection.cpp
        ${PROTO_SRC}
        ${PROTO_HDR}
    )
endif()

add_executable(dlsd
    ${DLSD_SOURCES}
)

set_target_properties(dlsd PROPERTIES
    CXX_STANDARD 17
    CXX_STANDARD_REQUIRED 1
)

target_link_libraries(dlsd PRIVATE
    dls
    EtherLab::pdcom5
    uriparser::uriparser
    Threads::Threads
    ${LIBXML2_LIBRARIES}
)

target_compile_options(dlsd PRIVATE -Wall)

target_include_directories(dlsd PRIVATE
    "${CMAKE_CURRENT_SOURCE_DIR}/.."
    ${CMAKE_CURRENT_BINARY_DIR}
    ${LIBXML2_INCLUDE_DIR}
)

if (!WIN32)
    target_link_libraries(dlsd PRIVATE -lm)
endif()

if (DLS_SERVER)
    target_link_libraries(dlsd PRIVATE
        protobuf::libprotobuf
    )
endif()

install(TARGETS dlsd
    RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}"
)
