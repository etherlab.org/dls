/****************************************************************************
**
**
****************************************************************************/

#include <QApplication>
#include <QDebug>
#include <QGuiApplication>
#include <QtQml>
#include <QtQuick/QQuickView>
#include "dlswidget.h"

int main(int argc, char** argv)
{
  QApplication app(argc, argv);
  QQmlApplicationEngine* engine = new QQmlApplicationEngine(&app);

  qmlRegisterType<DlsWidget>("de.igh.dls", 1, 0, "DlsWidget");

  engine->load(QUrl("app.qml"));

  if (engine->rootObjects().isEmpty()) {
    return -1;
  }

  return app.exec();
} 
